<?php


class M_peramalan extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_data_peramalan()
	{
		$this->db->select('*');
		$this->db->from('peramalan');
		$this->db->join('jenis_pepaya', 'id_jenis_pepaya=id_jenis', 'left');
		$this->db->join('user', 'id_user=ditambahkan_oleh', 'left');
		$this->db->order_by('periode', 'DESC');
		$this->db->order_by('tanggal_ditambahkan', 'DESC');
		return $this->db->get();
	}

	public function get_data_peramalan_per_jenis_periode($id_jenis, $periode)
	{
		$this->db->select('*');
		$this->db->from('peramalan');
		$this->db->join('jenis_pepaya', 'id_jenis_pepaya=id_jenis', 'left');
		$this->db->join('user', 'id_user=ditambahkan_oleh', 'left');
		$this->db->where('id_jenis_pepaya', $id_jenis);
		$this->db->where('periode', $periode);
		$this->db->order_by('tanggal_ditambahkan', 'DESC');
		return $this->db->get();
	}

	public function insert_peramalan($data)
	{
		return $this->db->insert('peramalan', $data);
	}
}