<?php


class M_history_stok_bibit extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function insert_history_stok($data_history)
	{
		return $this->db->insert('history_stok_bibit', $data_history);
	}

	public function get_data_history_per_jenis($id_stok)
	{
		$this->db->select('*');
		$this->db->from('history_stok_bibit');
		$this->db->join('jenis_pepaya', 'id_jenis_pepaya=id_jenis', 'left');
		$this->db->join('user', 'id_user=edited_by', 'left');
		$this->db->where('id_stok_bibit', $id_stok);
		$this->db->order_by('tanggal', 'DESC');
		return $this->db->get();
	}
}
