<?php


class M_stok_biji extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_data_stok_biji()
	{
		$this->db->select('*');
		$this->db->from('stok_biji');
		$this->db->join('jenis_pepaya', 'id_jenis_pepaya=id_jenis', 'left');
		$this->db->order_by('periode', 'DESC');
		return $this->db->get();
	}

	public function get_data_stok_per_jenis($id_stok)
	{
		$this->db->select('*');
		$this->db->from('jenis_pepaya');
		$this->db->join('stok_biji', 'id_jenis_pepaya=id_jenis', 'left');
		$this->db->where('id_stok', $id_stok);
		return $this->db->get();
	}

	public function update_stok($data_stok, $id_jenis, $id_stok)
	{
		$this->db->where('id_stok', $id_stok);
		$this->db->where('id_jenis_pepaya', $id_jenis);
		return $this->db->update('stok_biji', $data_stok);
	}

	public function insert_stok($data_stok)
	{
		return $this->db->insert('stok_biji', $data_stok);
	}

	public function cek_stok($id_jenis, $periode)
	{
		$this->db->select('*');
		$this->db->from('stok_biji');
		$this->db->where('id_jenis_pepaya', $id_jenis);
		$this->db->where('periode', $periode);
		return $this->db->get();
	}
}
