<?php


class M_jenis_pepaya extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_data_jenis()
	{
		$this->db->select('*');
		$this->db->from('jenis_pepaya');
		return $this->db->get();
	}

	public function get_data_jenis_per_id($id_jenis)
	{
		$this->db->select('*');
		$this->db->from('jenis_pepaya');
		$this->db->where('id_jenis', $id_jenis);
		return $this->db->get();
	}

	public function insert_jenis($data)
	{
		return $this->db->insert('jenis_pepaya', $data);
	}

	public function update_jenis($id_jenis, $data)
	{
		$this->db->where('id_jenis', $id_jenis);
		return $this->db->update('jenis_pepaya', $data);
	}

	public function delete_jenis($id_jenis)
	{
		$this->db->where('id_jenis', $id_jenis);
		return $this->db->delete('jenis_pepaya');
	}
}
