<?php


class M_user extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function cek_login($u,$p){
		$this->db->from('user');
		$this->db->where('username',$u);
		$this->db->where('password',$p);
		$a = $this->db->get();

		if($a->num_rows() == 1){
			$data = $a->result_array();
			$this->session->set_userdata('id_user', $data[0]['id_user']);
			$this->session->set_userdata('username', $data[0]['username']);
			$this->session->set_userdata('role', $data[0]['role']);
			$this->session->set_userdata('nama', $data[0]['nama']);
			return true;
		}
		else{
			return false;
		}
	}

	public function get_data_user()
	{
		$this->db->select('*');
		$this->db->from('user');
		return $this->db->get();
	}

	public function get_data_user_per_id($id_user)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id_user', $id_user);
		return $this->db->get();
	}

	public function insert_user($data)
	{
		return $this->db->insert('user', $data);
	}

	public function update_user($id_user, $data)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->update('user', $data);
	}

	public function delete_user($id_user)
	{
		$this->db->where('id_user', $id_user);
		return $this->db->delete('user');
	}
}
