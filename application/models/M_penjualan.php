<?php


class M_penjualan extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_data_penjualan()
	{
		$this->db->select('*');
		$this->db->from('penjualan');
		$this->db->join('jenis_pepaya', 'id_jenis=id_jenis_pepaya', 'left');
		$this->db->join('user', 'ditambahkan_oleh=id_user', 'left');
		$this->db->order_by('tanggal', 'ASC');
		return $this->db->get();
	}

	public function get_data_penjualan_diproses()
	{
		$this->db->select('*');
		$this->db->from('penjualan');
		$this->db->join('jenis_pepaya', 'id_jenis=id_jenis_pepaya', 'left');
		$this->db->join('user', 'ditambahkan_oleh=id_user', 'left');
		$this->db->where('status', '1');
		$this->db->order_by('tanggal', 'ASC');
		return $this->db->get();
	}

	public function get_data_penjualan_per_id($id_penjualan)
	{
		$this->db->select('*');
		$this->db->from('penjualan');
		$this->db->join('jenis_pepaya', 'id_jenis=id_jenis_pepaya', 'left');
		$this->db->join('user', 'ditambahkan_oleh=id_user', 'left');
		$this->db->where('id_penjualan', $id_penjualan);
		$this->db->order_by('tanggal', 'ASC');
		return $this->db->get();
	}

	public function insert_penjualan($data)
	{
		return $this->db->insert('penjualan', $data);
	}

	public function update_penjualan($id_penjualan, $data)
	{
		$this->db->where('id_penjualan', $id_penjualan);
		return $this->db->update('penjualan', $data);
	}

	public function get_data_penjualan_perbulan($periode, $jenis_pepaya)
	{
		$this->db->select("sum(jumlah) as jumlah, DATE_FORMAT(tanggal,'%Y-%m') as periode");
		$this->db->from('penjualan');
		$this->db->join('jenis_pepaya', 'id_jenis=id_jenis_pepaya', 'left');
		$this->db->join('user', 'ditambahkan_oleh=id_user', 'left');
		$this->db->where("DATE_FORMAT(tanggal,'%Y-%m')", $periode);
		$this->db->where('id_jenis_pepaya', $jenis_pepaya);
		$this->db->group_by('MONTH(tanggal), YEAR(tanggal)');
		$this->db->order_by('tanggal', 'ASC');
		return $this->db->get();
	}

	public function get_data_penjualan_peramalan($periode, $jenis_pepaya)
	{
		$this->db->select("sum(jumlah) as jumlah, DATE_FORMAT(tanggal,'%Y-%m') as periode");
		$this->db->from('penjualan');
		$this->db->join('jenis_pepaya', 'id_jenis=id_jenis_pepaya', 'left');
		$this->db->join('user', 'ditambahkan_oleh=id_user', 'left');
		$this->db->where("DATE_FORMAT(tanggal,'%Y-%m')", $periode);
		$this->db->where('id_jenis_pepaya', $jenis_pepaya);
		$this->db->where('status', 2);
		$this->db->group_by('MONTH(tanggal), YEAR(tanggal)');
		$this->db->order_by('tanggal', 'ASC');
		return $this->db->get();
	}
}