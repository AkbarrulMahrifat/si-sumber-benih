<?php


class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('M_user');

	}

	public function index() //tampilan halaman login
	{
		if ($this->session->userdata('id_user') != NULL) {
			return $this->redirect();
		} else {
			return $this->load->view('login.php');
		}
	}

	function aksi_login() //proses login
	{
		$u = $this->input->post('username');
		$p = $this->input->post('password');

		$b = $this->M_user->cek_login($u, $p);
		if ($b) {
			$this->redirect();
		} else {
			$this->session->set_flashdata('error', 'username dan password salah');
			redirect('/');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('nama');

		session_destroy();

		redirect('Login/index');
	}

	function redirect()
	{
		if ($this->session->userdata('role') == 1)
		{
			$this->session->set_flashdata('success', 'Login berhasil');
			redirect('Pemilik/beranda');
		}
		else if ($this->session->userdata('role') == 2)
		{
			$this->session->set_flashdata('success', 'Login berhasil');
			redirect('Penjualan/beranda');
		}
		else if ($this->session->userdata('role') == 3)
		{
			$this->session->set_flashdata('success', 'Login berhasil');
			redirect('Stok/beranda');
		}
	}
}
