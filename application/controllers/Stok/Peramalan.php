<?php


class Peramalan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 3)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_penjualan');
			$this->load->model('M_peramalan');
		}

	}

	public function index() //view history peramalan dan form peramalan
	{
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis()->result();
		$data['peramalan'] = $this->M_peramalan->get_data_peramalan()->result();
		$this->load->view('Stok/peramalan', $data);
	}

	public function buat_peramalan() //view dan proses hasil peramalan
	{
		$periode_awal = date('Y-m', strtotime($this->input->get('periode_awal')));
		$periode_peramalan = date('Y-m', strtotime($this->input->get('periode_peramalan')));
		$jenis_pepaya = $this->input->get('jenis_pepaya');
		//cari selisih bulan
		$timeStart = strtotime($periode_awal);
		$timeEnd = strtotime($periode_peramalan);
		// Menambah bulan ini + semua bulan pada tahun sebelumnya
		$date_diff = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
		// menghitung selisih bulan
		$date_diff += date("m",$timeEnd)-date("m",$timeStart);

		if ($periode_awal >= $periode_peramalan)
		{
			$this->session->set_flashdata('error', 'Periode Awal Stok harus lebih kecil dari Periode Peramalan');
			redirect('Stok/Peramalan');
		}
		elseif ($date_diff < 1)
		{
			$this->session->set_flashdata('error', 'Jumlah periode harus lebih dari 1 bulan');
			redirect('Stok/Peramalan');
		}
		else
		{
			//ambil periode per bulan
			$periode = $this->get_periode($periode_awal, $periode_peramalan);

			//ambil data aktual per bulan
			$aktual = array();
			foreach ($periode as $key => $p){
				$jumlah = $this->M_penjualan->get_data_penjualan_peramalan($p, $jenis_pepaya)->row();
				if ($jumlah != null){
					$aktual[$key] = (int) $jumlah->jumlah;
				}else{
					$aktual[$key] = 0;
				}
			}

			//ambil data peramalan
			$ARRSES = $this->get_ARRSES($periode, $aktual);

			//hitung MAPE
			$MAPE = $this->get_MAPE($periode, $ARRSES);

			//cari index MAPE terkecil
			$mape_terkecil = min($MAPE);
			$index_mape_terkecil = array_search($mape_terkecil, $MAPE);

			$data['pepaya'] = $this->M_jenis_pepaya->get_data_jenis_per_id($jenis_pepaya)->row();
			$data['periode'] = $periode;
			$data['aktual'] = $aktual;
			$data['ARRSES'] = $ARRSES;
			$data['MAPE'] = $MAPE;
			$data['index_mape_terkecil'] = $index_mape_terkecil;

			$this->load->view('Stok/hasil_peramalan', $data);
		}
	}

	public function get_periode($periode_awal, $periode_peramalan) //get periode peramalan
	{
		//cari selisih bulan
		$timeStart = strtotime($periode_awal);
		$timeEnd = strtotime($periode_peramalan);
		// Menambah bulan ini + semua bulan pada tahun sebelumnya
		$months = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
		// menghitung selisih bulan
		$months += date("m",$timeEnd)-date("m",$timeStart);

		//set periode
		$periode = array();
		for ($i = 0; $i <= $months; $i++)
		{
			$periode[] = date('Y-m', strtotime('+'.$i.' month', strtotime($periode_awal)));
		}

		return $periode;
	}

	public function get_ARRSES($periode, $aktual) //perhitungan ARRSES
	{
		$ARRSES = array();
		$beta = array('0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7', '0.8', '0.9');
		foreach ($beta as $key => $b)
		{
			for ($i=0; $i<count($periode); $i++){
				//kondisi jika periode pertama
				if ($i == 0)
				{
					$ARRSES[$key][$i]['periode'] = $periode[$i];
					$ARRSES[$key][$i]['aktual'] = $aktual[$i];
					$ARRSES[$key][$i]['hasil'] = 0;
					$ARRSES[$key][$i]['e'] = 0;
					$ARRSES[$key][$i]['E'] = 0;
					$ARRSES[$key][$i]['EA'] = 0;
					$ARRSES[$key][$i]['beta'] = 0;
					$ARRSES[$key][$i]['alpha'] = 0;
					$ARRSES[$key][$i]['PE'] = 0;
				}
				//kondisi jika periode kedua
				elseif ($i == 1)
				{
					$ARRSES[$key][$i]['periode'] = $periode[$i];
					$ARRSES[$key][$i]['aktual'] = $aktual[$i];
					$ARRSES[$key][$i]['hasil'] = $aktual[$i-1];
					$ARRSES[$key][$i]['e'] = $aktual[$i] - $ARRSES[$key][$i]['hasil'];
					$ARRSES[$key][$i]['E'] = ( $b * $ARRSES[$key][$i]['e'] ) + ( ( 1-$b ) * $ARRSES[$key][$i-1]['E'] );
					$ARRSES[$key][$i]['EA'] = ( $b * abs($ARRSES[$key][$i]['e']) ) + ( ( 1-$b ) * $ARRSES[$key][$i-1]['EA'] );
					$ARRSES[$key][$i]['beta'] = $b;
					$ARRSES[$key][$i]['alpha'] = $b;
					$ARRSES[$key][$i]['PE'] = abs( ( ($aktual[$i] - $ARRSES[$key][$i]['hasil']) / $aktual[$i] ) * 100 );
				}
				//kondisi jika periode terakhir/periode peramalan
				elseif ($i == (count($periode)-1))
				{
					$ARRSES[$key][$i]['periode'] = $periode[$i];
					$ARRSES[$key][$i]['aktual'] = 0;
					$ARRSES[$key][$i]['hasil'] = ( $ARRSES[$key][$i-1]['alpha'] * $ARRSES[$key][$i-1]['aktual'] ) + ( ( 1 - $ARRSES[$key][$i-1]['alpha'] ) * $ARRSES[$key][$i-1]['hasil'] );
					$ARRSES[$key][$i]['e'] = 0;
					$ARRSES[$key][$i]['E'] = 0;
					$ARRSES[$key][$i]['EA'] = 0;
					$ARRSES[$key][$i]['beta'] = $b;
					$ARRSES[$key][$i]['alpha'] = abs($ARRSES[$key][$i-1]['E'] / $ARRSES[$key][$i-1]['EA']);
					$ARRSES[$key][$i]['PE'] = 0;
				}
				//kondisi jika bukan periode pertama,kedua, dan terakhir/periode peramalan
				else
				{
					$ARRSES[$key][$i]['periode'] = $periode[$i];
					$ARRSES[$key][$i]['aktual'] = $aktual[$i];
					$ARRSES[$key][$i]['hasil'] = ( $ARRSES[$key][$i-1]['alpha'] * $ARRSES[$key][$i-1]['aktual'] ) + ( ( 1 - $ARRSES[$key][$i-1]['alpha'] ) * $ARRSES[$key][$i-1]['hasil'] );
					$ARRSES[$key][$i]['e'] = $aktual[$i] - $ARRSES[$key][$i]['hasil'];
					$ARRSES[$key][$i]['E'] = ( $b * $ARRSES[$key][$i]['e'] ) + ( ( 1-$b ) * $ARRSES[$key][$i-1]['E'] );
					$ARRSES[$key][$i]['EA'] = ( $b * abs($ARRSES[$key][$i]['e']) ) + ( ( 1-$b ) * $ARRSES[$key][$i-1]['EA'] );
					$ARRSES[$key][$i]['beta'] = $b;
					$ARRSES[$key][$i]['alpha'] = abs($ARRSES[$key][$i-1]['E'] / $ARRSES[$key][$i-1]['EA']);
					$ARRSES[$key][$i]['PE'] = abs( ( ($aktual[$i] - $ARRSES[$key][$i]['hasil']) / $aktual[$i] ) * 100 );
				}
			}
		}

		return $ARRSES;
	}

	public function get_MAPE($periode, $ARRSES) //perhitungan MAPE
	{
		$PE = array();
		$jumlah_pe = array();
		$sum_pe = array();
		$count_pe = array();
		$MAPE = array();

		//looping per beta
		for ($i=0; $i<count($ARRSES); $i++)
		{
			for ($j=0; $j<count($periode); $j++)
			{
				$PE[$i][$j] = $ARRSES[$i][$j]['PE'];

				if ($j != 0 && $j != (count($periode)-1))
				{
					$jumlah_pe[$i][$j] = $ARRSES[$i][$j]['PE'];
				}
			}
			if (count($ARRSES[$i]) > 2 && $jumlah_pe[$i] != NULL)
			{
				$sum_pe[$i] = array_sum($jumlah_pe[$i]);
				$count_pe[$i] = count($jumlah_pe[$i]);
				$MAPE[$i] = $sum_pe[$i] / $count_pe[$i];
			}
			else
			{
				$MAPE[$i] = 0;
			}
		}

		return $MAPE;
	}
	
	public function simpan_peramalan() //proses simpan hasil peramalan
	{
		$data = array(
			'id_jenis_pepaya' => $this->input->post('id_jenis_pepaya'),
			'periode' => $this->input->post('periode'),
			'hasil' => $this->input->post('hasil'),
			'PE' => $this->input->post('PE'),
			'MAPE' => $this->input->post('MAPE'),
			'ditambahkan_oleh' => $this->session->userdata('id_user'),
			'tanggal_ditambahkan' => date('Y-m-d H:i:s'),
		);

		$this->db->trans_start();
		$insert = $this->M_peramalan->insert_peramalan($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menambah data');
			redirect('Stok/Peramalan');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menambah data');
			redirect('Stok/Peramalan');
		}
	}
}