<?php


class Profile extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 3)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_user');
		}
	}

	public function index()
	{
		$id_user = $this->session->userdata('id_user');
		$data['user'] = $this->M_user->get_data_user_per_id($id_user)->first_row();
		$this->load->view('Stok/profile', $data);
	}
}