<?php


class Stok_biji extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 3)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_stok_biji');
			$this->load->model('M_history_stok_biji');
			$this->load->model('M_peramalan');
		}

	}

	public function index() //view daftar stok biji
	{
		$stok = $this->M_stok_biji->get_data_stok_biji()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}

			$peramalan = $this->M_peramalan->get_data_peramalan_per_jenis_periode($s->id_jenis, $s->periode)->first_row();
			if ($peramalan != NULL)
			{
				$s->rekomendasi = round($peramalan->hasil*100/80, 0);
			}
			else
			{
				$s->rekomendasi = '-';
			}
		}
		$data['stok'] = $stok;
		$this->load->view('Stok/daftar_stok_biji', $data);
	}

	public function tambah_periode_biji() //view tambah periode stok
	{
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis()->result();
		$this->load->view('Stok/tambah_periode_biji', $data);
	}

	public function insert_periode_biji() //proses simpan periode stok
	{
		$id_jenis = $this->input->post('jenis_pepaya');
		$periode = $this->input->post('periode');

		$data = array(
			'id_jenis_pepaya' => $id_jenis,
			'periode' => $periode,
			'stok' => 0,
		);

		$cek = $this->M_stok_biji->cek_stok($id_jenis, $periode);

		if ($cek->num_rows() > 0)
		{
			$this->session->set_flashdata('error', 'Periode stok sudah ada');
			redirect('Stok/Stok_biji/tambah_periode_biji');
		}

		$this->db->trans_start();
		$insert_stok = $this->M_stok_biji->insert_stok($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menambah data');
			redirect('Stok/Stok_biji/insert_periode_biji');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menambah data');
			redirect('Stok/Stok_biji');
		}
	}

	public function tambah_stok($id_stok) //menambah stok
	{
		$stok = $this->M_stok_biji->get_data_stok_per_jenis($id_stok)->row();
		if ($stok->stok == NULL)
		{
			$stok->stok = 0;
		}
		$data['stok'] = $stok;
		$this->load->view('Stok/tambah_stok_biji', $data);
	}

	public function kurangi_stok($id_stok) //mengurangi stok
	{
		$stok = $this->M_stok_biji->get_data_stok_per_jenis($id_stok)->row();
		if ($stok->stok == NULL)
		{
			$stok->stok = 0;
		}
		$data['stok'] = $stok;
		$this->load->view('Stok/kurangi_stok_biji', $data);
	}

	public function update_stok() //proses simpan ubah stok
	{
		$id_jenis = $this->input->post('id_jenis');
		$id_stok = $this->input->post('id_stok');
		$stok_awal = $this->input->post('stok');
		$stok_sisa = $this->input->post('sisa_stok');
		$stok_debit = $this->input->post('debit_stok');
		$stok_kredit = $this->input->post('kredit_stok');

		//cek untuk perubahan stok apakah update atau insert
		if ($stok_debit == NULL)
		{
			$stok_debit = 0;
		}
		if ($stok_kredit == NULL)
		{
			$stok_kredit = 0;
		}

		$this->db->trans_start();
		$data_stok = array(
			'stok' => $stok_sisa
		);
		$update_stok = $this->M_stok_biji->update_stok($data_stok, $id_jenis, $id_stok);

		$data_history = array(
			'id_stok_biji' => $id_stok,
			'id_jenis_pepaya' => $id_jenis,
			'stok_awal' => $stok_awal,
			'debit' => $stok_debit,
			'kredit' => $stok_kredit,
			'stok_sisa' => $stok_sisa,
			'keterangan' => 'Diubah oleh Admin Stok.',
			'edited_by' => $this->session->userdata('id_user'),
			'tanggal' => date('Y-m-d H:i:s'),
		);
		$insert_history = $this->M_history_stok_biji->insert_history_stok($data_history);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal mengubah data');
			redirect('Stok/Stok_biji');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil mengubah data');
			redirect('Stok/Stok_biji');
		}
	}

	public function lihat_history($id_stok) //lihat history stok
	{
		$data['history'] = $this->M_history_stok_biji->get_data_history_per_jenis($id_stok)->result();
		$this->load->view('Stok/history_stok_biji', $data);
	}
}
