<?php


class Jenis_pepaya extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 3)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_jenis_pepaya');
		}
	}

	public function index() //daftar jenispepaya
	{
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis()->result();
		$this->load->view('Stok/daftar_jenis', $data);
	}

	public function tambah_jenis() //view tambah jenis pepaya
	{
		$this->load->view('Stok/tambah_jenis');
	}

	public function insert_jenis() //proses simpan tambah jenis pepaya
	{
		$data = array(
			'nama_jenis' => $this->input->post('nama_jenis'),
		);

		$this->db->trans_start();
		$insert = $this->M_jenis_pepaya->insert_jenis($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menambah data');
			redirect('Stok/Jenis_pepaya/tambah_jenis/');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menambah data');
			redirect('Stok/Jenis_pepaya');
		}
	}

	public function edit_jenis($id_jenis) //view ubah jenis pepaya
	{
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis_per_id($id_jenis)->row();
		$this->load->view('Stok/ubah_jenis', $data);
	}

	public function update_jenis() //proses simpan ubah jenis pepaya
	{
		$data = array(
			'nama_jenis' => $this->input->post('nama_jenis'),
		);
		$id_jenis = $this->input->post('id_jenis');
		$this->db->trans_start();
		$update = $this->M_jenis_pepaya->update_jenis($id_jenis, $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal mengubah data');
			redirect('Stok/Jenis_pepaya/edit_jenis/' . $id_jenis);
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil mengubah data');
			redirect('Stok/Jenis_pepaya');
		}
	}

	public function hapus_jenis($id_jenis) //hapus jenis pepaya
	{
		$this->db->trans_start();
		$delete = $this->M_jenis_pepaya->delete_jenis($id_jenis);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menghapus data');
			redirect('Stok/Jenis_pepaya');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menghapus data');
			redirect('Stok/Jenis_pepaya');
		}
	}
}
