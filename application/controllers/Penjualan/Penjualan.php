<?php


class Penjualan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 2)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_stok_bibit');
			$this->load->model('M_history_stok_bibit');
			$this->load->model('M_penjualan');
		}

	}

	public function index() //halaman penjualan
	{
		$stok = $this->M_stok_bibit->get_data_stok_bibit()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}
		}
		$data['pepaya'] = $stok;
		$data['penjualan'] = $this->M_penjualan->get_data_penjualan_diproses()->result();
		$this->load->view('Penjualan/penjualan', $data);
	}

	public function daftar_penjualan() //halaman rekap penjualan
	{
		$stok = $this->M_stok_bibit->get_data_stok_bibit()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}
		}
		$data['pepaya'] = $stok;
		$data['penjualan'] = $this->M_penjualan->get_data_penjualan()->result();
		$this->load->view('Penjualan/daftar_penjualan', $data);
	}

	public function buat_pesanan($id_jenis, $id_stok)//view tambah pesanan
	{
		$periode1 = date('Y-m');
		$periode2 = date('Y-m', strtotime('-1 month'));
		$pepaya = $this->M_jenis_pepaya->get_data_jenis_per_id($id_jenis)->row();
		$get_stok = $this->M_stok_bibit->get_data_stok_per_jenis($id_stok)->row();
		$stok = 0;
		if ($get_stok)
		{
			$stok = $get_stok->stok;
		}
		$pepaya->stok = $stok;

		$data['pepaya'] = $pepaya;
		$data['id_stok'] = $id_stok;
		$data['tanggal'] = $get_stok->periode;
		$this->load->view('Penjualan/buat_pesanan', $data);
	}

	public function insert_pesanan() //proses simpan tambah pesanan
	{
		$id_stok = $this->input->post('id_stok');
		$id_jenis = $this->input->post('id_jenis');

		$data_pesanan = array(
			'id_jenis_pepaya' => $id_jenis,
			'id_stok_bibit' => $id_stok,
			'nama_pemesan' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'tanggal' => $this->input->post('tanggal'),
			'status' => '1',
			'ditambahkan_oleh' => $this->session->userdata('id_user'),
		);

		$stok_sisa = intval($this->input->post('stok')) - intval($this->input->post('jumlah'));
		$data_stok = array(
			'stok' => $stok_sisa
		);

		$data_history = array(
			'id_stok_bibit' => $id_stok,
			'id_jenis_pepaya' => $id_jenis,
			'stok_awal' => $this->input->post('stok'),
			'debit' => 0,
			'kredit' => $this->input->post('jumlah'),
			'stok_sisa' => $stok_sisa,
			'keterangan' => 'Penjualan tanggal '.$this->input->post('tanggal').' atas nama '.$this->input->post('nama'),
			'edited_by' => $this->session->userdata('id_user'),
			'tanggal' => date('Y-m-d H:i:s'),
		);

		$this->db->trans_start();
		$update_stok = $this->M_stok_bibit->update_stok($data_stok, $id_jenis, $id_stok);
		$insert_history = $this->M_history_stok_bibit->insert_history_stok($data_history);
		$insert_penjualan = $this->M_penjualan->insert_penjualan($data_pesanan);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal membuat pesanan');
			redirect('Penjualan/penjualan');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil membuat pesanan');
			redirect('Penjualan/penjualan');
		}
	}

	public function ubah_pesanan($id_penjualan) //view ubah pesanan
	{
		$penjualan = $this->M_penjualan->get_data_penjualan_per_id($id_penjualan)->row();

		$pepaya = $this->M_jenis_pepaya->get_data_jenis_per_id($penjualan->id_jenis_pepaya)->row();
		$get_stok = $this->M_stok_bibit->get_data_stok_per_jenis($penjualan->id_stok_bibit)->row();
		$stok = 0;
		if ($get_stok)
		{
			$stok = $get_stok->stok;
		}
		$pepaya->stok = $stok;

		$data['pepaya'] = $pepaya;
		$data['penjualan'] = $penjualan;
		$data['id_stok'] = $penjualan->id_stok_bibit;
		$data['tanggal'] = $get_stok->periode;
		$this->load->view('Penjualan/ubah_pesanan', $data);
	}

	public function update_pesanan() //proses simpan ubah pesanan
	{
		$id_stok = $this->input->post('id_stok');
		$id_jenis = $this->input->post('id_jenis');
		$id_penjualan = $this->input->post('id_penjualan');

		$data_pesanan = array(
			'id_jenis_pepaya' => $id_jenis,
			'id_stok_bibit' => $id_stok,
			'nama_pemesan' => $this->input->post('nama'),
			'jumlah' => $this->input->post('jumlah'),
			'tanggal' => $this->input->post('tanggal'),
			'status' => '1',
			'ditambahkan_oleh' => $this->session->userdata('id_user'),
		);

		$stok_awal = intval($this->input->post('stok')) + intval($this->input->post('jumlah_lama'));
		$stok_sisa = $stok_awal - intval($this->input->post('jumlah'));
		$data_stok = array(
			'stok' => $stok_sisa
		);

		$data_history = array(
			'id_stok_bibit' => $id_stok,
			'id_jenis_pepaya' => $id_jenis,
			'stok_awal' => $stok_awal,
			'debit' => 0,
			'kredit' => $this->input->post('jumlah'),
			'stok_sisa' => $stok_sisa,
			'keterangan' => 'Perubahan Penjualan tanggal '.$this->input->post('tanggal').' atas nama '.$this->input->post('nama'),
			'edited_by' => $this->session->userdata('id_user'),
			'tanggal' => date('Y-m-d H:i:s'),
		);

		$this->db->trans_start();
		$update_stok = $this->M_stok_bibit->update_stok($data_stok, $id_jenis, $id_stok);
		$insert_history = $this->M_history_stok_bibit->insert_history_stok($data_history);
		$update_penjualan = $this->M_penjualan->update_penjualan($id_penjualan, $data_pesanan);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal mengubah pesanan');
			redirect('Penjualan/penjualan');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil mengubah pesanan');
			redirect('Penjualan/penjualan');
		}
	}

	public function ubah_status($id_penjualan, $status) //ubah status pesanan
	{
		$data = array(
			'status' => $status
		);

		$this->db->trans_start();
		$update_status = $this->M_penjualan->update_penjualan($id_penjualan, $data);

		if ($status == 0)
		{
			//jika dibatalkan, proses mengembalikan stok
			$penjualan = $this->M_penjualan->get_data_penjualan_per_id($id_penjualan)->row();
			$stok = $this->M_stok_bibit->get_data_stok_per_jenis($penjualan->id_stok_bibit)->row();
			$stok_sisa = $penjualan->jumlah + $stok->stok;

			$data_stok = array(
				'stok' => $stok_sisa
			);
			$update_stok = $this->M_stok_bibit->update_stok($data_stok, $penjualan->id_jenis_pepaya, $penjualan->id_stok_bibit);

			$data_history = array(
				'id_stok_bibit' => $penjualan->id_stok_bibit,
				'id_jenis_pepaya' => $penjualan->id_jenis_pepaya,
				'stok_awal' => $stok->stok,
				'debit' => $penjualan->jumlah,
				'kredit' => 0,
				'stok_sisa' => $stok_sisa,
				'keterangan' => 'Pengembalian stok dari penjualan yang dibatalkan, tanggal '.$penjualan->tanggal.' atas nama '.$penjualan->nama_pemesanan,
				'edited_by' => $this->session->userdata('id_user'),
				'tanggal' => date('Y-m-d H:i:s'),
			);
			$insert_history = $this->M_history_stok_bibit->insert_history_stok($data_history);
		}
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal mengubah status pesanan');
			redirect('Penjualan/penjualan');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil mengubah status pesanan');
			redirect('Penjualan/penjualan');
		}
	}
}