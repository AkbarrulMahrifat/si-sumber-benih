<?php


class Beranda extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 2)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_user');
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_penjualan');
			$this->load->model('M_peramalan');
		}

	}

	public function index()
	{
		$periode_awal = $this->input->get('periode_awal');
		$periode_akhir = $this->input->get('periode_akhir');
		$jenis_pepaya = $this->input->get('jenis_pepaya');

		if (date('Y-m', strtotime($periode_awal)) > date('Y-m', strtotime($periode_akhir)))
		{
			$this->session->set_flashdata('error', 'Periode Awal harus lebih kecil dari Periode Akhir');
			redirect('Penjualan/Beranda');
		}

		if (!$periode_awal){
			$periode_awal = date('Y-m', strtotime('-12 month'));
		}
		if (!$periode_akhir){
			$periode_akhir = date('Y-m');
		}
		if (!$jenis_pepaya){
			$jenis_pepaya = $this->M_jenis_pepaya->get_data_jenis()->first_row()->id_jenis;
		}

		$data_jenis_pepaya = $this->M_jenis_pepaya->get_data_jenis_per_id($jenis_pepaya)->row();

		$periode = $this->get_periode($periode_awal, $periode_akhir);

		//ambil data aktual per bulan
		$aktual = array();
		foreach ($periode[0] as $key => $p){
			$jumlah = $this->M_penjualan->get_data_penjualan_perbulan($p, $jenis_pepaya)->row();
			if ($jumlah != null){
				$aktual[$key] = (int) $jumlah->jumlah;
			}else{
				$aktual[$key] = 0;
			}
		}
		//ambil data peramalan per bulan
		$peramalan = array();
		foreach ($periode[0] as $key => $p){
			$jumlah = $this->M_peramalan->get_data_peramalan_per_jenis_periode($jenis_pepaya, $p)->row();
			if ($jumlah != null){
				$peramalan[$key] = (int) $jumlah->hasil;
			}else{
				$peramalan[$key] = 0;
			}
		}

		$data['periode_awal'] = date('M Y', strtotime($periode_awal));
		$data['periode_akhir'] = date('M Y', strtotime($periode_akhir));
		$data['jenis_pepaya'] = $data_jenis_pepaya;
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis()->result();
		$data['periode'] = json_encode($periode[1]);
		$data['aktual'] = json_encode($aktual);
		$data['peramalan'] = json_encode($peramalan);

		$this->load->view('Penjualan/beranda', $data);
	}

	public function get_periode($periode_awal, $periode_akhir)
	{
		//cari selisih bulan
		$timeStart = strtotime($periode_awal);
		$timeEnd = strtotime($periode_akhir);
		// Menambah bulan ini + semua bulan pada tahun sebelumnya
		$months = (date("Y",$timeEnd)-date("Y",$timeStart))*12;
		// menghitung selisih bulan
		$months += date("m",$timeEnd)-date("m",$timeStart);

		//set periode
		$periode = array();
		$periode_text = array();
		for ($i = 0; $i <= $months; $i++)
		{
			$periode[] = date('Y-m', strtotime('+'.$i.' month', strtotime($periode_awal)));
			$periode_text[] = date('M Y', strtotime($periode[$i]));
		}

		$array_periode = array($periode, $periode_text);
		return $array_periode;
	}
}
