<?php


class Penjualan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 1)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_user');
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_stok_bibit');
			$this->load->model('M_history_stok_bibit');
			$this->load->model('M_penjualan');
		}

	}

	public function index()
	{
		$stok = $this->M_stok_bibit->get_data_stok_bibit()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}
		}
		$data['pepaya'] = $stok;
		$data['penjualan'] = $this->M_penjualan->get_data_penjualan()->result();
		$this->load->view('Pemilik/rekap_penjualan', $data);
	}
}