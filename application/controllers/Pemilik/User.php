<?php


class User extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 1)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_user');
		}

	}

	public function index() //view daftar user
	{
		$data['user'] = $this->M_user->get_data_user()->result();
		$this->load->view('Pemilik/daftar_user', $data);
	}

	public function tambah_user() //view tambah user
	{
		$this->load->view('Pemilik/tambah_user');
	}

	public function insert_user() //proses simpan tambah user
	{
		$data = array(
			'nama' => $this->input->post('nama'),
			'no_telp' => $this->input->post('no_telp'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('jabatan'),
		);

		$this->db->trans_start();
		$update = $this->M_user->insert_user($data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menambah data');
			redirect('Pemilik/User/tambah_user/');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menambah data');
			redirect('Pemilik/User');
		}
	}

	public function edit_user($id_user)
	{
		$data['user'] = $this->M_user->get_data_user_per_id($id_user)->row();
		$this->load->view('Pemilik/ubah_user', $data);
	}

	public function update_user()
	{
		$data = array(
			'nama' => $this->input->post('nama'),
			'no_telp' => $this->input->post('no_telp'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('jabatan'),
		);
		$id_user = $this->input->post('id_user');
		$this->db->trans_start();
		$update = $this->M_user->update_user($id_user, $data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal mengubah data');
			redirect('Pemilik/User/edit_profile/' . $id_user);
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil mengubah data');
			redirect('Pemilik/User');
		}
	}

	public function hapus_user($id_user) //delete user
	{
		$this->db->trans_start();
		$delete = $this->M_user->delete_user($id_user);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$this->session->set_flashdata('error', 'Gagal menghapus data');
			redirect('Pemilik/User');
		}
		else
		{
			$this->session->set_flashdata('success', 'Berhasil menghapus data');
			redirect('Pemilik/User');
		}
	}
}
