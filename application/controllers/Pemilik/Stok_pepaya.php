<?php


class Stok_pepaya extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('role') != 1)
		{
			redirect('Login/logout');
		}
		else{
			$this->load->model('M_jenis_pepaya');
			$this->load->model('M_stok_biji');
			$this->load->model('M_history_stok_biji');
			$this->load->model('M_stok_bibit');
			$this->load->model('M_history_stok_bibit');
			$this->load->model('M_peramalan');
		}
	}

	public function daftar_jenis()
	{
		$data['jenis'] = $this->M_jenis_pepaya->get_data_jenis()->result();
		$this->load->view('Pemilik/daftar_jenis', $data);
	}

	public function stok_biji()
	{
		$stok = $this->M_stok_biji->get_data_stok_biji()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}

			$peramalan = $this->M_peramalan->get_data_peramalan_per_jenis_periode($s->id_jenis, $s->periode)->first_row();
			if ($peramalan != NULL)
			{
				$s->rekomendasi = round($peramalan->hasil*100/80, 0);
			}
			else
			{
				$s->rekomendasi = '-';
			}
		}
		$data['stok'] = $stok;
		$this->load->view('Pemilik/daftar_stok_biji', $data);
	}

	public function lihat_history_stok_biji($id_stok)
	{
		$data['history'] = $this->M_history_stok_biji->get_data_history_per_jenis($id_stok)->result();
		$this->load->view('Pemilik/history_stok_biji', $data);
	}

	public function stok_bibit()
	{
		$stok = $this->M_stok_bibit->get_data_stok_bibit()->result();
		foreach ($stok as $s)
		{
			if ($s->stok == NULL)
			{
				$s->stok = 0;
			}

			$peramalan = $this->M_peramalan->get_data_peramalan_per_jenis_periode($s->id_jenis, $s->periode)->first_row();
			if ($peramalan != NULL)
			{
				$s->rekomendasi = round($peramalan->hasil, 2);
			}
			else
			{
				$s->rekomendasi = '-';
			}
		}
		$data['stok'] = $stok;
		$this->load->view('Pemilik/daftar_stok_bibit', $data);
	}

	public function lihat_history_stok_bibit($id_stok)
	{
		$data['history'] = $this->M_history_stok_bibit->get_data_history_per_jenis($id_stok)->result();
		$this->load->view('Pemilik/history_stok_bibit', $data);
	}

	public function lihat_hasil_peramalan()
	{
		$data['peramalan'] = $this->M_peramalan->get_data_peramalan()->result();
		$this->load->view('Pemilik/peramalan', $data);
	}
}
