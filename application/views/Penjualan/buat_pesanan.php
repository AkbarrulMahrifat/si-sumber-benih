<!--Load Header-->
<?php $this->load->view('Penjualan/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Penjualan</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Buat Pesanan Stok Bibit <strong>"<?=$pepaya->nama_jenis?>"</strong></h3>
						</div>
						<div class="panel-body">
							<form method="post" action="<?=site_url('Penjualan/penjualan/insert_pesanan')?>">
								<input type="hidden" id="id_jenis" name="id_jenis" value="<?=$pepaya->id_jenis?>">
								<input type="hidden" id="id_stok" name="id_stok" value="<?=$id_stok?>">
								<div class="form-group col-md-6">
									<label for="nama">Nama Pemesan</label>
									<input type="text" class="form-control" id="nama" placeholder="Nama Pemesan" name="nama" required>
								</div>
								<div class="form-group col-md-6">
                                    <label for="datetime1">Tanggal Pemesanan</label>
                                    <input type="text" class="form-control" id="datetime1" placeholder="Jumlah Pesanan" name="tanggal" value="<?=$tanggal.'-'.date('d')?>" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="stok">Stok Saat Ini</label>
                                    <input type="text" class="form-control" id="stok" placeholder="Stok Bibit Pepaya" name="stok" value="<?=$pepaya->stok?>" readonly>
                                </div>
                                <div class="form-group col-md-6">
									<label for="jumlah">Jumlah Pesanan</label>
									<input type="number" class="form-control" id="jumlah" placeholder="Jumlah Pesanan" name="jumlah" max="<?=$pepaya->stok?>" required>
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a class="btn btn-danger" href="<?=site_url('Stok/Stok_bibit')?>">Kembali</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
	//hitung sisa ketika kolom jumlah diisi
	$('#jumlah').on('input', function () {
        var stok = $('#stok').val();
        var jumlah = $('#jumlah').val();

		var sisa_stok = parseInt(stok) - parseInt(jumlah);

		if (sisa_stok >= 0){
            $('#sisa_stok').val(sisa_stok);
        } else {
		    alert('Jumlah pesanan tidak boleh lebih besar dari stok yang tersedia.')
		}
    });
</script>

<!--Load Footer-->
<?php $this->load->view('Penjualan/footer.php')?>
