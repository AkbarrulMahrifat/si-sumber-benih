<!doctype html>
<html lang="en">

<head>
	<title><?=ucwords(str_replace("_"," ",$this->uri->segment(2)))?> | SI Sumber Benih</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/vendor/chartist/css/chartist-custom.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/vendor/toastr/toastr.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/demo.css">
	<!--DataTables-->
	<link href="<?=base_url()?>assets/DataTables/datatables.css" rel="stylesheet" />
    <!--datetimepicker-->
    <link rel="stylesheet" href="<?=base_url()?>assets/datepicker/css/bootstrap-datepicker.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?=base_url()?>assets/img/favicon.png">
	<!-- JQuery -->
	<script src="<?=base_url()?>assets/vendor/jquery/jquery.min.js"></script>
</head>

<body>
<!-- WRAPPER -->
<div id="wrapper">
	<!-- NAVBAR -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="brand">
			<a href="<?=base_url()?>"><img src="<?=base_url()?>assets/img/logo-dark.png" width="160" alt="Klorofil Logo" class="img-responsive logo"></a>
		</div>
		<div class="container-fluid">
			<div class="navbar-btn">
				<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
			</div>
			<div id="navbar-menu">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?=base_url()?>assets/img/user.png" class="img-circle" alt="Avatar"> <span><?=$this->session->userdata('nama')?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
						<ul class="dropdown-menu">
							<li><a href="<?=site_url('Penjualan/profile')?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
							<li><a href="<?=site_url('Login/logout')?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- END NAVBAR -->
	<!-- LEFT SIDEBAR -->
	<div id="sidebar-nav" class="sidebar">
		<div class="sidebar-scroll">
			<nav>
				<ul class="nav">
					<li><a href="<?=site_url('Penjualan/beranda')?>"><i class="lnr lnr-home"></i> <span>Beranda</span></a></li>
					<li><a href="<?=site_url('Penjualan/penjualan')?>"><i class="lnr lnr-cart"></i> <span>Penjualan</span></a></li>
					<li><a href="<?=site_url('Penjualan/penjualan/daftar_penjualan')?>"><i class="lnr lnr-book"></i> <span>Daftar Penjualan</span></a></li>
<!--					<li><a href="elements.html" class=""><i class="lnr lnr-code"></i> <span>Elements</span></a></li>-->
<!--					<li><a href="charts.html" class=""><i class="lnr lnr-chart-bars"></i> <span>Charts</span></a></li>-->
<!--					<li><a href="panels.html" class=""><i class="lnr lnr-cog"></i> <span>Panels</span></a></li>-->
<!--					<li><a href="notifications.html" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li>-->
<!--					<li>-->
<!--						<a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i> <span>Pages</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>-->
<!--						<div id="subPages" class="collapse ">-->
<!--							<ul class="nav">-->
<!--								<li><a href="page-profile.html" class="">Profile</a></li>-->
<!--								<li><a href="page-login.html" class="">Login</a></li>-->
<!--								<li><a href="page-lockscreen.html" class="">Lockscreen</a></li>-->
<!--							</ul>-->
<!--						</div>-->
<!--					</li>-->
<!--					<li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>-->
<!--					<li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>-->
<!--					<li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li>-->
				</ul>
			</nav>
		</div>
	</div>
	<!-- END LEFT SIDEBAR -->
