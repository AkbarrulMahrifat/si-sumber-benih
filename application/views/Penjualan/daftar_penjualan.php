<!--Load Header-->
<?php $this->load->view('Penjualan/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Penjualan</h3>

            <!-- Daftar Penjualan -->
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Daftar Penjualan</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Pemesan</th>
                                    <th>Jenis Pepaya</th>
                                    <th>Jumlah Pesanan</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Ditambahkan Oleh</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								$no = 1;
								foreach ($penjualan as $p) { ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$p->nama_pemesan?></td>
                                        <td><?=$p->nama_jenis?></td>
                                        <td><?=$p->jumlah?></td>
                                        <td><?=$p->tanggal?></td>
                                        <td>
                                            <?php
                                                if ($p->status == 1)
                                                {
                                                    echo '<span class="label label-primary">Diproses</span>';
                                                }
                                                elseif ($p->status == 2)
                                                {
													echo '<span class="label label-success">Selesai</span>';
                                                }
                                                elseif ($p->status == 0)
                                                {
													echo '<span class="label label-danger">Dibatalkan</span>';
                                                }
                                            ?>
                                        </td>
                                        <td><?=$p->nama?></td>
                                        <td>
											<?php
											if ($p->status == 1)
											{
												echo '
												<a class="text-primary" style="font-size: large; margin-right: 10px" title="Ubah Pesanan" href="' . site_url('Penjualan/Penjualan/ubah_pesanan/'.$p->id_penjualan.'/2'). '"><i class="fa fa-edit"></i></a>
												<a class="text-success" style="font-size: large; margin-right: 10px" title="Ubah Status Selesai" href="' . site_url('Penjualan/Penjualan/ubah_status/'.$p->id_penjualan.'/2'). '"><i class="fa fa-check"></i></a>
                                                <a class="text-danger" style="font-size: large; margin-right: 10px" title="Ubah Status Dibatalkan" href="' . site_url('Penjualan/Penjualan/ubah_status/'.$p->id_penjualan.'/0') . '"><i class="fa fa-close"></i></a>
                                                ';
                                            }
                                            elseif ($p->status == 2 or $p->status == 0)
											{
												echo '-';
											}
                                            else
											{
												echo '
												<a class="text-primary" style="font-size: large; margin-right: 10px" title="Ubah Status Diproses" href="' . site_url('Penjualan/Penjualan/ubah_status/'.$p->id_penjualan.'/1') . '"><i class="fa fa-clock-o"></i></a>
												';
											}
											?>
                                        </td>
                                    </tr>
								<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Penjualan/footer.php')?>
