<!--Load Header-->
<?php $this->load->view('Pemilik/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Penjualan</h3>

            <!-- Daftar Penjualan -->
            <div class="row">
                <div class="col-md-12">
                    <!-- TABLE HOVER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Rekap Penjualan</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="datatable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Pemesan</th>
                                    <th>Jenis Pepaya</th>
                                    <th>Jumlah Pesanan</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Ditambahkan Oleh</th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								$no = 1;
								foreach ($penjualan as $p) { ?>
                                    <tr>
                                        <td><?=$no++?></td>
                                        <td><?=$p->nama_pemesan?></td>
                                        <td><?=$p->nama_jenis?></td>
                                        <td><?=$p->jumlah?></td>
                                        <td><?=$p->tanggal?></td>
                                        <td>
                                            <?php
                                                if ($p->status == 1)
                                                {
                                                    echo '<span class="label label-primary">Diproses</span>';
                                                }
                                                elseif ($p->status == 2)
                                                {
													echo '<span class="label label-success">Selesai</span>';
                                                }
                                                elseif ($p->status == 0)
                                                {
													echo '<span class="label label-danger">Dibatalkan</span>';
                                                }
                                            ?>
                                        </td>
                                        <td><?=$p->nama?></td>
                                    </tr>
								<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
                </div>
            </div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Pemilik/footer.php')?>
