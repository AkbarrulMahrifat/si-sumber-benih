
<div class="clearfix"></div>
<footer>
	<div class="container-fluid">
		<p class="copyright">&copy; 2020 <a href="<?=base_url()?>" target="_blank">SI Sumber Benih</a>. All Rights Reserved.</p>
	</div>
</footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="<?=base_url()?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/vendor/toastr/toastr.min.js"></script>
<script src="<?=base_url()?>assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="<?=base_url()?>assets/vendor/chartjs/Chart.bundle.min.js"></script>
<script src="<?=base_url()?>assets/vendor/chartist/js/chartist.min.js"></script>
<script src="<?=base_url()?>assets/vendor/chartist/plugins/chartist-plugin-legend.js"></script>
<script src="<?=base_url()?>assets/scripts/klorofil-common.js"></script>
<!--DataTables-->
<script src="<?=base_url()?>assets/DataTables/datatables.js"></script>
<!--datepicker-->
<script src="<?=base_url()?>assets/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable();
        $('#datatable2').DataTable();

        $('#month1').datepicker({
            format: "yyyy-mm",
            startView: 1,
            minViewMode: 1,
            maxViewMode: 2,
            clearBtn: true,
            autoclose: true,
            orientation: "bottom"
        });
        $('#month2').datepicker({
            format: "yyyy-mm",
            startView: 1,
            minViewMode: 1,
            maxViewMode: 2,
            clearBtn: true,
            autoclose: true,
            orientation: "bottom"
        });
    });

	<?php if ($this->session->flashdata('success')){?>
    test('success', '<?=$this->session->flashdata('success')?>');
	<?php } ?>
	<?php if ($this->session->flashdata('error')){?>
    test('error', '<?=$this->session->flashdata('error')?>');
	<?php } ?>

    function test(context, massage)
    {
        $context = context;
        $message = massage;
        $position = 'toast-top-right';

        toastr.remove();
        toastr[$context]($message, '' , { positionClass: $position });
    }
</script>
</body>

</html>
