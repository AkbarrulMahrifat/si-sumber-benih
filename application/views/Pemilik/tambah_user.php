<!--Load Header-->
<?php $this->load->view('Pemilik/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">User</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Tambah Profil Pengguna</h3>
						</div>
						<div class="panel-body">
							<form method="post" action="<?=site_url('Pemilik/User/insert_user')?>">
								<div class="form-group">
									<label for="nama">Nama Pengguna</label>
									<input type="text" class="form-control" id="nama" placeholder="Nama Pengguna" name="nama" required>
								</div>
								<div class="form-group">
									<label for="no_telp">Nomor Telepon</label>
									<input type="text" class="form-control" id="no_telp" placeholder="Nomor Telepon" name="no_telp" required>
								</div>
								<div class="form-group">
									<label for="username">Username</label>
									<input type="text" class="form-control" id="username" placeholder="Username" name="username" required>
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="text" class="form-control" id="password" placeholder="Password" name="password" required>
								</div>
								<div class="form-group">
									<label for="jabatan">Jabatan</label>
									<select class="form-control" id="jabatan" name="jabatan" required>
										<option value="">-- Pilih Jabatan --</option>
										<option value="1">Pemilik</option>
										<option value="2">Admin Penjualan</option>
										<option value="3">Admin Stok</option>
									</select>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
								<a class="btn btn-danger" href="<?=site_url('Pemilik/User')?>">Kembali</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
	//set change ketika value option sama dengan data
</script>

<!--Load Footer-->
<?php $this->load->view('Pemilik/footer.php')?>
