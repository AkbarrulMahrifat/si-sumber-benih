<!--Load Header-->
<?php $this->load->view('Pemilik/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Stok Bibit Pepaya</h3>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Daftar Stok Bibit Pepaya</h3>
                        </div>
						<div class="panel-body">
							<table class="table table-hover" id="datatable">
								<thead>
								<tr>
									<th>#</th>
									<th>Jenis Pepaya</th>
									<th>Periode</th>
									<th>Stok</th>
									<th>Rekomendasi</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($stok as $j) { ?>
								<tr>
									<td><?=$no++?></td>
									<td><?=$j->nama_jenis?></td>
									<td><?=$j->periode?></td>
									<td><?=$j->stok?></td>
									<td><?=$j->rekomendasi?></td>
									<td>
										<a class="text-info" style="font-size: large; margin-right: 10px" title="History Stok" href="<?=site_url('Pemilik/Stok_pepaya/lihat_history_stok_bibit/'.$j->id_stok)?>"><i class="fa fa-eye"></i></a>
                                    </td>
								</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TABLE HOVER -->
				</div>
			</div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Pemilik/footer.php')?>
