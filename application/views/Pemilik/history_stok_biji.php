<!--Load Header-->
<?php $this->load->view('Pemilik/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Stok Biji Pepaya</h3>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">History Stok Biji Pepaya</h3>
						</div>
						<div class="panel-body">
							<table class="table table-hover" id="datatable">
								<thead>
								<tr>
									<th>#</th>
									<th>Jenis Pepaya</th>
									<th>Tanggal</th>
									<th>Stok Awal</th>
									<th>Debit Stok</th>
									<th>Kredit Stok</th>
									<th>Stok Sisa</th>
									<th>Keterangan</th>
									<th>Diubah Oleh</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($history as $h) { ?>
								<tr>
									<td><?=$no++?></td>
									<td><?=$h->nama_jenis?></td>
									<td><?=$h->tanggal?></td>
									<td><?=$h->stok_awal?></td>
									<td><?=$h->debit?></td>
									<td><?=$h->kredit?></td>
									<td><?=$h->stok_sisa?></td>
									<td><?=$h->keterangan?></td>
									<td><?=$h->nama?></td>
								</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TABLE HOVER -->
				</div>
			</div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Pemilik/footer.php')?>
