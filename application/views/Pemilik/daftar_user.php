<!--Load Header-->
<?php $this->load->view('Pemilik/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">User</h3>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Daftar Pengguna</h3>
							<a class="btn btn-sm btn-success pull-right" style="margin-bottom: 10px" title="Tambah" href="<?=site_url('Pemilik/User/tambah_user')?>"><i class="fa fa-plus"></i> Tambah</a>
						</div>
						<div class="panel-body">
							<table class="table table-hover" id="datatable">
								<thead>
								<tr>
									<th>#</th>
									<th>Nama</th>
									<th>Nomor Telepon</th>
									<th>Username</th>
									<th>Password</th>
									<th>Jabatan</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($user as $u) { ?>
								<tr>
									<td><?=$no++?></td>
									<td><?=$u->nama?></td>
									<td><?=$u->no_telp?></td>
									<td><?=$u->username?></td>
									<td><?=$u->password?></td>
									<td>
										<?php
										if ($u->role == 1)
										{
											echo "Pemilik";
										}
										else if ($u->role == 2)
										{
											echo "Admin Penjualan";
										}
										else if ($u->role == 3)
										{
											echo "Admin Stok";
										}
										else
										{
											echo "-";
										}
										?>
									</td>
									<td>
										<a class="text-primary" style="font-size: large; margin-right: 10px" title="Ubah" href="<?=site_url('Pemilik/User/edit_user/'.$u->id_user)?>"><i class="fa fa-edit"></i></a>
										<a class="text-danger" style="font-size: large; margin-right: 10px" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Pemilik/User/hapus_user/'.$u->id_user)?>"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TABLE HOVER -->
				</div>
			</div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Pemilik/footer.php')?>
