<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Jenis Pepaya</h3>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Daftar Jenis Pepaya</h3>
							<a class="btn btn-sm btn-success pull-right" style="margin-bottom: 10px" title="Tambah" href="<?=site_url('Stok/Jenis_pepaya/tambah_jenis')?>"><i class="fa fa-plus"></i> Tambah</a>
						</div>
						<div class="panel-body">
							<table class="table table-hover" id="datatable">
								<thead>
								<tr>
									<th>#</th>
									<th>Jenis Pepaya</th>
									<th>Aksi</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($jenis as $j) { ?>
								<tr>
									<td><?=$no++?></td>
									<td><?=$j->nama_jenis?></td>
									<td>
										<a class="text-primary" style="font-size: large; margin-right: 10px" title="Ubah" href="<?=site_url('Stok/Jenis_pepaya/edit_jenis/'.$j->id_jenis)?>"><i class="fa fa-edit"></i></a>
										<a class="text-danger" style="font-size: large; margin-right: 10px" title="Hapus" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')" href="<?=site_url('Stok/Jenis_pepaya/hapus_jenis/'.$j->id_jenis)?>"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TABLE HOVER -->
				</div>
			</div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
