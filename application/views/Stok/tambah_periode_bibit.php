<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Stok Bibit Pepaya</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Tambah Periode Stok Bibit</h3>
						</div>
						<div class="panel-body">
							<form method="post" action="<?=site_url('Stok/Stok_bibit/insert_periode_bibit')?>">
								<div class="form-group col-md-6">
									<label for="jenis_pepaya">Jenis Pepaya</label>
									<select class="form-control" id="jenis_pepaya" name="jenis_pepaya" required>
                                        <option value="">--- Pilih Jenis Pepaya ---</option>
                                        <?php foreach ($jenis as $j){?>
                                            <option value="<?=$j->id_jenis?>"><?=$j->nama_jenis?></option>
                                        <?php } ?>
                                    </select>
								</div>
								<div class="form-group col-md-6">
									<label for="month1">Periode</label>
									<input type="text" class="form-control" id="month1" placeholder="Periode Stok" name="periode" required>
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a class="btn btn-danger" href="<?=site_url('Stok/Stok_bibit')?>">Kembali</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
	//hitung sisa ketika kolom stok diisi
	$('#debit_stok').on('input', function () {
        var stok = $('#stok').val();
        var debit = $('#debit_stok').val();

		var sisa_stok = parseInt(stok) + parseInt(debit);

		if (sisa_stok >= 0){
            $('#sisa_stok').val(sisa_stok);
        } else {
            $('#sisa_stok').val('0');
		}
    });
</script>

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
