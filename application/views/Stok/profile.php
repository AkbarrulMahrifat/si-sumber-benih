<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-profile">
                <div class="clearfix">
                    <!-- PROFILE HEADER -->
                    <div class="profile-header">
                        <div class="overlay"></div>
                        <div class="profile-main">
                            <img src="<?=base_url()?>assets/img/user-medium.png" class="img-circle" alt="Avatar">
                            <h3 class="name"><?=$user->nama?></h3>
                            <span class="online-status status-available">Available</span>
                        </div>
                    </div>
                    <!-- END PROFILE HEADER -->
                    <!-- PROFILE DETAIL -->
                    <div class="profile-detail">
                        <div class="profile-info text-center">
                            <h3 class="heading">Detail Profile</h3>
                            <br>
                            <ul class="list-unstyled list-justify">
                                <li><strong>Nama Lengkap : </strong><?=$user->nama?></li>
                                <li><strong>Username : </strong><?=$user->username?></li>
                                <li><strong>Password : </strong><?=$user->password?></li>
                                <li><strong>No. Telepon : </strong><?=$user->no_telp?></li>
                                <li>
                                    <strong>Jabatan : </strong>
                                    <?php
                                    if ($user->role == 1)
                                    {
                                        echo "Pemilik";
                                    }
                                    elseif ($user->role == 2)
                                    {
                                        echo "Admin Penjualan";
                                    }
                                    elseif ($user->role == 3)
                                    {
                                        echo "Admin Stok";
                                    }
                                        ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- END PROFILE DETAIL -->
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
    $(document).ready(function() {
        chart();
    });

    function chart(){
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?=$periode?>,
                datasets: [{
                    label: 'Data Aktual',
                    data: <?=$aktual?>,
                    backgroundColor:
                        'rgba(255, 99, 132, 0.2)',
                    borderColor:
                        'rgba(255,99,132,1)',
                    borderWidth: 1
                },
                    {
                        label: 'Data Peramalan',
                        data: <?=$peramalan?>,
                        backgroundColor:
                            'rgba(54, 162, 235, 0.2)',
                        borderColor:
                            'rgba(54, 162, 235, 1)',
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                scales: {
                    xAxes: [{
                        ticks: {
                            maxRotation: 90,
                            minRotation: 80
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
</script>

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
