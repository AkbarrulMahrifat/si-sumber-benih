<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Hasil Peramalan</h3>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Hasil Rekomendasi Peramalan
                                <strong><?=$pepaya->nama_jenis?></strong>
                                Periode
                                <strong><?=$periode[count($periode)-1]?></strong>
                            </h3>
						</div>
						<div class="panel-body">
							<table class="table table-hover">
								<thead>
								<tr>
									<th>#</th>
									<th>Metode</th>
                                    <th>Beta</th>
                                    <th>Alpha</th>
                                    <th>Hasil</th>
									<th>MAPE</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								$class = '';
								for ($i=0; $i<count($ARRSES); $i++) {
								if ($i == $index_mape_terkecil)
                                {
                                    $class = 'class="bg-success" style="color: black"';
                                }else{
									$class = '';
                                }
								?>
								<tr <?=$class?>>
									<td><?=$no++;?></td>
									<td>ARRSES Beta <?=number_format($ARRSES[$i][(count($ARRSES[$i])-1)]['beta'], 1, ',', '.')?></td>
                                    <td><?=number_format($ARRSES[$i][(count($ARRSES[$i])-1)]['beta'], 1, ',', '.')?></td>
                                    <td><?=number_format($ARRSES[$i][(count($ARRSES[$i])-1)]['alpha'], 2, ',', '.')?></td>
									<td><?=number_format($ARRSES[$i][(count($ARRSES[$i])-1)]['hasil'], 2, ',', '.')?></td>
									<td><?=number_format($MAPE[$i], 2, ',', '.')?></td>
								</tr>
								<?php } ?>
								</tbody>
							</table>

                            <hr>

                            <div class="col-md-12">
                                <p><strong>Rekomendasi :</strong></p>
                                <p>
                                    Metode peramalan dengan MAPE terkecil adalah <strong>ARRSES Beta <?=$ARRSES[$index_mape_terkecil][(count($ARRSES[$index_mape_terkecil])-1)]['beta']?></strong>,
                                    dengan hasil peramalan <strong><?=number_format($ARRSES[$index_mape_terkecil][(count($ARRSES[$index_mape_terkecil])-1)]['hasil'], 2, ',', '.')?></strong>
                                    dan MAPE <strong><?=number_format($MAPE[$index_mape_terkecil], 2, ',', '.')?>%</strong>.
                                </p>
                                <p>
                                    Hasil rekomendasi stok bibit <strong><?=$pepaya->nama_jenis?></strong>
                                    untuk periode <strong><?=$periode[count($periode)-1]?></strong> adalah
                                    <strong><?=number_format($ARRSES[$index_mape_terkecil][(count($ARRSES[$index_mape_terkecil])-1)]['hasil'], 0, ',', '.')?></strong> bibit.
                                </p>
                            </div>

                            <br>

                            <div class="col-md-12 text-left">
                                <form action="<?=site_url('Stok/Peramalan/simpan_peramalan')?>" method="post">
                                    <input type="hidden" name="id_jenis_pepaya" value="<?=$pepaya->id_jenis?>">
                                    <input type="hidden" name="periode" value="<?=$periode[count($periode)-1]?>">
                                    <input type="hidden" name="hasil" value="<?=round($ARRSES[$index_mape_terkecil][(count($ARRSES[$index_mape_terkecil])-1)]['hasil'], 2)?>">
                                    <input type="hidden" name="PE" value="<?=round($ARRSES[$index_mape_terkecil][(count($ARRSES[$index_mape_terkecil])-1)]['PE'], 2)?>">
                                    <input type="hidden" name="MAPE" value="<?=round($MAPE[$index_mape_terkecil], 2)?>">
                                    <button class="btn btn-success" type="submit">Simpan</button>
                                    <a class="btn btn-danger" href="<?=site_url('Stok/Peramalan')?>">Kembali</a>
                                </form>
                            </div>

                        </div>
                    </div>
                    <!-- END TABLE HOVER -->
				</div>
			</div>

            <div class="row">
                <div class="col-md-12">
                    <!-- PANEL DEFAULT -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detail Peramalan</h3>
                        </div>
                        <div class="panel-body">
                            <div class="accrodion-regular">
                                <div id="accordion3">
									<?php
									$no = 1;
									$class = '';
									for ($i=0; $i<count($ARRSES); $i++) {
									?>
                                    <!--Detail Peramalan-->
                                    <div class="card mb-2">
                                        <div class="card-header" id="headingEight">
                                            <h5 class="mb-0 text-left">
                                                <a class="btn btn-block bg-primary collapsed" data-toggle="collapse" data-target="#beta_<?=$i?>" aria-expanded="false" aria-controls="collapseEight">
                                                    <span class="lnr lnr-chevron-down"></span> ARRSES Beta <?=$ARRSES[$i][(count($ARRSES[$i])-1)]['beta']?>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="beta_<?=$i?>" class="collapse" aria-labelledby="headingEight" data-parent="#accordion3">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead class="bg-primary">
                                                        <tr>
                                                            <th>Periode</th>
                                                            <th>Aktual</th>
                                                            <th>e</th>
                                                            <th>E</th>
                                                            <th>EA</th>
                                                            <th>Alpha</th>
                                                            <th>Beta</th>
                                                            <th>Hasil</th>
                                                            <th>PE</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php for ($j=0; $j<count($ARRSES[$i]); $j++){?>
                                                        <tr>
                                                            <td><?=$ARRSES[$i][$j]['periode']?></td>
                                                            <td><?=$ARRSES[$i][$j]['aktual']?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['e'], 2, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['E'], 2, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['EA'], 2, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['alpha'], 2, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['beta'], 1, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['hasil'], 2, ',', '.')?></td>
                                                            <td><?=number_format($ARRSES[$i][$j]['PE'], 2, ',', '.')?></td>
                                                        </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                        <tfoot class="bg-info">
                                                        <tr>
                                                            <th colspan="7"></th>
                                                            <th>MAPE</th>
                                                            <td><?=number_format($MAPE[$i], 2, ',', '.')?></td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End Detail Peramalan-->
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL DEFAULT -->
                </div>
            </div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
