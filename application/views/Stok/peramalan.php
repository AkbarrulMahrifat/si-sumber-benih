<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Peramalan</h3>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Buat Peramalan</h3>
                        </div>
                        <div class="panel-body">
                            <form method="get" action="<?=site_url('Stok/Peramalan/buat_peramalan')?>">
                                <div class="form-group col-md-4">
                                    <label for="jenis_pepaya">Jenis Pepaya</label>
                                    <select class="form-control" id="jenis_pepaya" name="jenis_pepaya" required>
                                        <option value="">--- Pilih Jenis Pepaya ---</option>
										<?php foreach ($jenis as $j){?>
                                            <option value="<?=$j->id_jenis?>"><?=$j->nama_jenis?></option>
										<?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="month1">Periode Awal Stok</label>
                                    <input type="text" class="form-control" id="month1" placeholder="Periode Awal" name="periode_awal" required>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="month2">Periode Peramalan</label>
                                    <input type="text" class="form-control" id="month2" placeholder="Periode Peramalan" name="periode_peramalan" required>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

			<div class="row">
				<div class="col-md-12">
					<!-- TABLE HOVER -->
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Riwayat Peramalan</h3>
						</div>
						<div class="panel-body">
							<table class="table table-hover" id="datatable">
								<thead>
								<tr>
									<th>#</th>
									<th>Jenis Pepaya</th>
									<th>Periode</th>
									<th>Hasil</th>
									<th>MAPE</th>
									<th>Rekomendasi Stok Bibit</th>
									<th>Rekomendasi Stok Biji</th>
									<th>Ditambahkan Oleh</th>
									<th>Tanggal Ditambahkan</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($peramalan as $p) { ?>
								<tr>
									<td><?=$no++?></td>
									<td><?=$p->nama_jenis?></td>
									<td><?=$p->periode?></td>
									<td><?=number_format($p->hasil, 2, ',', '.')?></td>
									<td><?=number_format($p->MAPE, 2, ',', '.')?></td>
									<td><?=number_format($p->hasil, 0, ',', '.')?></td>
									<td><?=number_format($p->hasil*100/80, 0, ',', '.')?></td>
                                    <td><?=$p->nama?></td>
                                    <td><?=$p->tanggal_ditambahkan?></td>
                                </tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- END TABLE HOVER -->
				</div>
			</div>

		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
