<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title">Grafik Penjualan x Peramalan</h3>
                    <p class="panel-subtitle">Jenis Pepaya : <?=$jenis_pepaya->nama_jenis?>, Periode: <?=$periode_awal?> - <?=$periode_akhir?></p>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form action="<?=site_url('Stok/beranda')?>" method="get">
                            <div class="col-md-12">
                                <p>Filter :</p>
                            </div>
                            <div class="form-group col-md-3">
                                <select class="form-control" id="jenis_pepaya" name="jenis_pepaya" required>
                                    <option value="">--- Pilih Jenis Pepaya ---</option>
									<?php foreach ($jenis as $j){?>
                                        <option value="<?=$j->id_jenis?>"><?=$j->nama_jenis?></option>
									<?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="month1" placeholder="Periode Awal" name="periode_awal" required>
                            </div>
                            <div class="form-group col-md-3">
                                <input type="text" class="form-control" id="month2" placeholder="Periode Akhir" name="periode_akhir" required>
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <!--							<div id="main-chart" class="ct-chart"></div>-->
                            <canvas id="myChart" width="987" height="305"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
    $(document).ready(function() {
        chart();
    });

    function chart(){
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?=$periode?>,
                datasets: [{
                    label: 'Data Aktual',
                    data: <?=$aktual?>,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    // fill:false,
                    // lineTension: 0
                },
                    {
                        label: 'Data Peramalan',
                        data: <?=$peramalan?>,
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1,
                        // fill:false,
                        // lineTension: 0
                    }]
            },
            options: {
                responsive: true,
                scales: {
                    xAxes: [{
                        ticks: {
                            maxRotation: 90,
                            minRotation: 80
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }
</script>

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
