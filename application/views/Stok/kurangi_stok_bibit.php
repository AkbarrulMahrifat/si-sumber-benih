<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Stok Bibit Pepaya</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Kurangi Stok Bibit <strong>"<?=$stok->nama_jenis?>"</strong></h3>
						</div>
						<div class="panel-body">
							<form method="post" action="<?=site_url('Stok/Stok_bibit/update_stok')?>">
								<input type="hidden" id="id_stok" name="id_stok" value="<?=$stok->id_stok?>">
								<input type="hidden" id="id_jenis" name="id_jenis" value="<?=$stok->id_jenis?>">
								<div class="form-group col-md-6">
									<label for="stok">Stok Saat Ini</label>
									<input type="text" class="form-control" id="stok" placeholder="Stok Bibit Pepaya" name="stok" value="<?=$stok->stok?>" readonly>
								</div>
								<div class="form-group col-md-6">
									<label for="sisa_stok">Sisa Stok</label>
									<input type="text" class="form-control" id="sisa_stok" placeholder="Sisa Stok Bibit Pepaya" name="sisa_stok" value="<?=$stok->stok?>" readonly>
								</div>
								<div class="form-group col-md-6">
									<label for="kredit_stok">Kurangi Stok</label>
									<input type="number" class="form-control" id="kredit_stok" placeholder="Kurangi Stok Bibit Pepaya" name="kredit_stok" min="0" max="<?=$stok->stok?>" required>
								</div>
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary">Simpan</button>
									<a class="btn btn-danger" href="<?=site_url('Stok/Stok_bibit')?>">Kembali</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
	//hitung sisa ketika kolom stok diisi
	$('#kredit_stok').on('input', function () {
        var stok = $('#stok').val();
        var kredit = $('#kredit_stok').val();

		var sisa_stok = parseInt(stok) - parseInt(kredit);

		if (sisa_stok >= 0){
            $('#sisa_stok').val(sisa_stok);
        } else {
            $('#sisa_stok').val('0');
		}
    });
</script>

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
