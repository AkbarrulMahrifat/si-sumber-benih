<!--Load Header-->
<?php $this->load->view('Stok/header.php')?>

<!-- MAIN -->
<div class="main">
	<!-- MAIN CONTENT -->
	<div class="main-content">
		<div class="container-fluid">
			<h3 class="page-title">Jenis Pepaya</h3>
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">Tambah Jenis Pepaya</h3>
						</div>
						<div class="panel-body">
							<form method="post" action="<?=site_url('Stok/Jenis_pepaya/insert_jenis')?>">
								<div class="form-group">
									<label for="nama_jenis">Nama Jenis</label>
									<input type="text" class="form-control" id="nama_jenis" placeholder="Nama Jenis Pepaya" name="nama_jenis" required>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
								<a class="btn btn-danger" href="<?=site_url('Stok/Jenis_pepaya')?>">Kembali</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<script>
	//set change ketika value option sama dengan data
</script>

<!--Load Footer-->
<?php $this->load->view('Stok/footer.php')?>
