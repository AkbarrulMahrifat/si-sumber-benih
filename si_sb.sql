-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2020 at 04:59 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si_sb`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_stok_bibit`
--

CREATE TABLE `history_stok_bibit` (
  `id_history` int(11) NOT NULL,
  `id_stok_bibit` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `debit` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `edited_by` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_stok_bibit`
--

INSERT INTO `history_stok_bibit` (`id_history`, `id_stok_bibit`, `id_jenis_pepaya`, `stok_awal`, `debit`, `kredit`, `stok_sisa`, `keterangan`, `edited_by`, `tanggal`) VALUES
(1, 1, 1, 0, 40, 0, 40, 'Diubah oleh Admin Stok.', 4, '2020-02-19 00:00:00'),
(2, 1, 1, 40, 10, 0, 50, 'Diubah oleh Admin Stok.', 4, '2020-02-19 00:00:00'),
(3, 1, 1, 50, 0, 15, 50, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:16:16'),
(4, 1, 1, 50, 0, 15, 35, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:18:25'),
(5, 2, 2, 0, 40, 0, 40, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:19:10'),
(6, 1, 1, 35, 100, 0, 135, 'Diubah oleh Admin Stok.', 4, '2020-02-25 21:54:23'),
(7, 3, 1, 0, 150, 0, 150, 'Diubah oleh Admin Stok.', 4, '2020-02-25 22:23:27'),
(8, 3, 1, 150, 0, 25, 125, 'Diubah oleh Admin Stok.', 4, '2020-02-25 22:27:59'),
(10, 3, 1, 125, 0, 25, 100, 'Penjualan tanggal 2020-01-28', 3, '2020-02-28 03:20:14'),
(11, 1, 1, 135, 0, 35, 100, 'Penjualan tanggal 2019-12-29', 3, '2020-02-29 16:50:44'),
(12, 3, 1, 100, 25, 0, 125, 'Pengembalian stok dari penjualan yang dibatalkan.', 3, '2020-02-29 17:13:46'),
(13, 3, 1, 125, 0, 25, 100, 'Penjualan tanggal 2020-01-14', 3, '2020-02-29 19:01:02'),
(15, 3, 1, 125, 0, 50, 75, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:28:42'),
(16, 3, 1, 100, 0, 50, 50, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:32:01'),
(17, 3, 1, 75, 0, 50, 25, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:32:16'),
(18, 3, 1, 50, 0, 50, 0, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:36:46'),
(19, 3, 1, 50, 0, 50, 0, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:36:52'),
(20, 3, 1, 50, 0, 50, 0, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:37:09'),
(21, 3, 1, 175, 0, 50, 125, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-02-29 23:43:11'),
(22, 3, 1, 175, 0, 25, 150, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:02:45'),
(23, 3, 1, 175, 0, 75, 100, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:03:49'),
(24, 3, 1, 175, 0, 75, 100, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:04:38'),
(25, 3, 1, 175, 0, 75, 100, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:05:34'),
(26, 3, 1, 175, 0, 75, 100, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:10:43'),
(27, 3, 1, 175, 0, 75, 100, 'Perubahan Penjualan tanggal 2020-01-14 atas nama Tini', 3, '2020-03-01 00:11:51'),
(28, 2, 2, 40, 60, 0, 100, 'Diubah oleh Admin Stok.', 4, '2020-03-01 00:24:56'),
(29, 3, 1, 100, 0, 0, 100, 'Diubah oleh Admin Stok.', 4, '2020-03-03 08:59:45'),
(30, 3, 1, 100, 0, 100, 0, 'Diubah oleh Admin Stok.', 4, '2020-03-03 09:00:16'),
(31, 3, 1, 0, 0, 0, 0, 'Penjualan tanggal 2020-01-03 atas nama Satuyah', 3, '2020-03-03 09:01:38'),
(32, 3, 1, 0, 0, 0, 0, 'Pengembalian stok dari penjualan yang dibatalkan, tanggal 2020-01-03 atas nama ', 3, '2020-03-03 09:01:47'),
(33, 3, 1, 0, 75, 0, 75, 'Pengembalian stok dari penjualan yang dibatalkan, tanggal 2020-01-14 atas nama ', 3, '2020-03-03 11:25:28'),
(34, 3, 1, 75, 15, 0, 90, 'Pengembalian stok dari penjualan yang dibatalkan, tanggal 2020-02-13 atas nama ', 3, '2020-03-03 11:25:34'),
(35, 3, 1, 90, 7000, 0, 7090, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:32:47'),
(36, 3, 1, 7090, 0, 90, 7000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:32:58'),
(37, 4, 1, 0, 7000, 0, 7000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:37:40'),
(38, 27, 2, 0, 6250, 0, 6250, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:47:17'),
(39, 15, 1, 0, 6750, 0, 6750, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:47:31'),
(40, 26, 2, 0, 6000, 0, 6000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:47:48'),
(41, 14, 1, 0, 6000, 0, 6000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:48:06'),
(42, 25, 2, 0, 5500, 0, 5500, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:48:21'),
(43, 13, 1, 0, 5850, 0, 5850, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:48:35'),
(44, 12, 1, 0, 5250, 0, 5250, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:49:30'),
(45, 24, 2, 0, 6000, 0, 6000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:49:47'),
(46, 11, 1, 0, 5500, 0, 5500, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:50:34'),
(47, 23, 2, 0, 7200, 0, 7200, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:50:57'),
(48, 10, 1, 0, 5450, 0, 5450, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:51:26'),
(49, 22, 2, 0, 6750, 0, 6750, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:51:45'),
(50, 21, 2, 0, 5250, 0, 5250, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:52:09'),
(51, 9, 1, 0, 5250, 0, 5250, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:53:21'),
(52, 8, 1, 0, 5000, 0, 5000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:53:43'),
(53, 20, 2, 0, 5500, 0, 5500, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:54:00'),
(54, 19, 2, 0, 5000, 0, 5000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:54:17'),
(55, 7, 1, 0, 5150, 0, 5150, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:54:40'),
(56, 18, 2, 0, 6000, 0, 6000, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:54:59'),
(57, 6, 1, 0, 4850, 0, 4850, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:55:21'),
(58, 16, 2, 0, 6500, 0, 6500, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:55:43'),
(59, 17, 2, 0, 6250, 0, 6250, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:55:59'),
(60, 5, 1, 0, 6500, 0, 6500, 'Diubah oleh Admin Stok.', 4, '2020-03-03 11:56:12'),
(61, 27, 2, 6250, 0, 6250, 0, 'Penjualan tanggal 2019-12-03 atas nama desi', 3, '2020-03-03 11:57:57'),
(62, 15, 1, 6750, 0, 6750, 0, 'Penjualan tanggal 2019-12-02 atas nama destia', 3, '2020-03-03 11:58:45'),
(63, 26, 2, 6000, 0, 6000, 0, 'Penjualan tanggal 2019-11-03 atas nama novi', 3, '2020-03-03 11:59:48'),
(64, 14, 1, 6000, 0, 6000, 0, 'Penjualan tanggal 2019-11-04 atas nama novan', 3, '2020-03-03 12:00:08'),
(65, 25, 2, 5500, 0, 5500, 0, 'Penjualan tanggal 2019-10-17 atas nama oka', 3, '2020-03-03 12:00:30'),
(66, 13, 1, 5850, 0, 5850, 0, 'Penjualan tanggal 2019-10-09 atas nama olin', 3, '2020-03-03 12:00:49'),
(67, 12, 1, 5250, 0, 5250, 0, 'Penjualan tanggal 2019-09-24 atas nama sela', 3, '2020-03-03 12:34:32'),
(68, 24, 2, 6000, 0, 6000, 0, 'Penjualan tanggal 2019-09-15 atas nama sinta', 3, '2020-03-03 12:34:49'),
(69, 11, 1, 5500, 0, 5500, 0, 'Penjualan tanggal 2019-08-20 atas nama agus', 3, '2020-03-03 12:35:07'),
(70, 23, 2, 7200, 0, 7200, 0, 'Penjualan tanggal 2019-08-03 atas nama budi', 3, '2020-03-03 12:35:20'),
(71, 10, 1, 5450, 0, 5450, 0, 'Penjualan tanggal 2019-07-27 atas nama agam', 3, '2020-03-03 12:36:07'),
(72, 22, 2, 6750, 0, 6750, 0, 'Penjualan tanggal 2019-07-14 atas nama yayan', 3, '2020-03-03 12:36:32'),
(73, 21, 2, 5250, 0, 5250, 0, 'Penjualan tanggal 2019-06-23 atas nama yayan', 3, '2020-03-03 12:37:00'),
(74, 9, 1, 5250, 0, 5250, 0, 'Penjualan tanggal 2019-06-05 atas nama desi', 3, '2020-03-03 12:37:23'),
(75, 8, 1, 5000, 0, 5000, 0, 'Penjualan tanggal 2019-05-07 atas nama novi', 3, '2020-03-03 12:37:46'),
(76, 20, 2, 5500, 0, 5500, 0, 'Penjualan tanggal 2019-05-30 atas nama agus', 3, '2020-03-03 12:44:34'),
(77, 19, 2, 5000, 0, 5000, 0, 'Penjualan tanggal 2019-04-01 atas nama yuli', 3, '2020-03-03 12:44:54'),
(78, 7, 1, 5150, 0, 5150, 0, 'Penjualan tanggal 2019-04-29 atas nama desi', 3, '2020-03-03 12:46:55'),
(79, 18, 2, 6000, 0, 6000, 0, 'Penjualan tanggal 2019-03-12 atas nama Ratna', 3, '2020-03-03 12:47:20'),
(80, 6, 1, 4850, 0, 4850, 0, 'Penjualan tanggal 2019-03-26 atas nama destia', 3, '2020-03-03 12:47:39'),
(81, 17, 2, 6250, 0, 6250, 0, 'Penjualan tanggal 2019-02-10 atas nama agus', 3, '2020-03-03 12:48:02'),
(82, 5, 1, 6500, 0, 6500, 0, 'Penjualan tanggal 2019-02-24 atas nama roni', 3, '2020-03-03 12:48:20'),
(83, 16, 2, 6500, 0, 6500, 0, 'Penjualan tanggal 2019-01-03 atas nama budi', 3, '2020-03-03 12:48:34'),
(84, 4, 1, 7000, 0, 7000, 0, 'Penjualan tanggal 2019-01-21 atas nama desi', 3, '2020-03-03 12:48:51'),
(85, 27, 2, 0, 1000, 0, 1000, 'Diubah oleh Admin Stok.', 4, '2020-03-05 14:46:37'),
(86, 27, 2, 1000, 0, 1000, 0, 'Penjualan tanggal 2019-12-05 atas nama destia', 3, '2020-03-05 14:48:43'),
(87, 27, 2, 0, 1000, 0, 1000, 'Pengembalian stok dari penjualan yang dibatalkan, tanggal 2019-12-05 atas nama ', 3, '2020-03-05 14:49:04');

-- --------------------------------------------------------

--
-- Table structure for table `history_stok_biji`
--

CREATE TABLE `history_stok_biji` (
  `id_history` int(11) NOT NULL,
  `id_stok_biji` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `stok_awal` int(11) NOT NULL,
  `debit` int(11) NOT NULL,
  `kredit` int(11) NOT NULL,
  `stok_sisa` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `edited_by` int(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_stok_biji`
--

INSERT INTO `history_stok_biji` (`id_history`, `id_stok_biji`, `id_jenis_pepaya`, `stok_awal`, `debit`, `kredit`, `stok_sisa`, `keterangan`, `edited_by`, `tanggal`) VALUES
(1, 0, 1, 0, 120, 0, 120, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:52:21'),
(2, 0, 1, 120, 0, 20, 100, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:52:29'),
(3, 2, 2, 0, 150, 0, 0, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:53:35'),
(4, 2, 2, 150, 0, 20, 130, 'Diubah oleh Admin Stok.', 4, '2020-02-19 23:54:07'),
(5, 3, 1, 0, 200, 0, 200, 'Diubah oleh Admin Stok.', 4, '2020-02-25 22:35:25'),
(6, 3, 1, 200, 0, 50, 150, 'Diubah oleh Admin Stok.', 4, '2020-02-25 22:35:29');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pepaya`
--

CREATE TABLE `jenis_pepaya` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pepaya`
--

INSERT INTO `jenis_pepaya` (`id_jenis`, `nama_jenis`) VALUES
(1, 'Pepaya California'),
(2, 'Pepaya Thailand');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `id_penjualan` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `id_stok_bibit` int(11) NOT NULL,
  `nama_pemesan` varchar(225) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(11) NOT NULL,
  `ditambahkan_oleh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`id_penjualan`, `id_jenis_pepaya`, `id_stok_bibit`, `nama_pemesan`, `jumlah`, `tanggal`, `status`, `ditambahkan_oleh`) VALUES
(6, 2, 27, 'desi', 6250, '2019-12-03', 2, 3),
(7, 1, 15, 'destia', 6750, '2019-12-02', 2, 3),
(8, 2, 26, 'novi', 6000, '2019-11-03', 2, 3),
(9, 1, 14, 'novan', 6000, '2019-11-04', 2, 3),
(10, 2, 25, 'oka', 5500, '2019-10-17', 2, 3),
(11, 1, 13, 'olin', 5850, '2019-10-09', 2, 3),
(12, 1, 12, 'sela', 5250, '2019-09-24', 2, 3),
(13, 2, 24, 'sinta', 6000, '2019-09-15', 2, 3),
(14, 1, 11, 'agus', 5500, '2019-08-20', 2, 3),
(15, 2, 23, 'budi', 7200, '2019-08-03', 2, 3),
(16, 1, 10, 'agam', 5450, '2019-07-27', 2, 3),
(17, 2, 22, 'yayan', 6750, '2019-07-14', 2, 3),
(18, 2, 21, 'yayan', 5250, '2019-06-23', 2, 3),
(19, 1, 9, 'desi', 5250, '2019-06-05', 2, 3),
(20, 1, 8, 'novi', 5000, '2019-05-07', 2, 3),
(21, 2, 20, 'agus', 5500, '2019-05-30', 2, 3),
(22, 2, 19, 'yuli', 5000, '2019-04-01', 2, 3),
(23, 1, 7, 'desi', 5150, '2019-04-29', 2, 3),
(24, 2, 18, 'Ratna', 6000, '2019-03-12', 2, 3),
(25, 1, 6, 'destia', 4850, '2019-03-26', 2, 3),
(26, 2, 17, 'agus', 6250, '2019-02-10', 2, 3),
(27, 1, 5, 'roni', 6500, '2019-02-24', 2, 3),
(28, 2, 16, 'budi', 6500, '2019-01-03', 2, 3),
(29, 1, 4, 'desi', 7000, '2019-01-21', 2, 3),
(30, 2, 27, 'destia', 1000, '2019-12-05', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `peramalan`
--

CREATE TABLE `peramalan` (
  `id_peramalan` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `periode` varchar(10) NOT NULL,
  `hasil` decimal(10,2) NOT NULL,
  `PE` decimal(10,2) NOT NULL,
  `MAPE` decimal(10,2) NOT NULL,
  `ditambahkan_oleh` int(11) NOT NULL,
  `tanggal_ditambahkan` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peramalan`
--

INSERT INTO `peramalan` (`id_peramalan`, `id_jenis_pepaya`, `periode`, `hasil`, `PE`, `MAPE`, `ditambahkan_oleh`, `tanggal_ditambahkan`) VALUES
(5, 1, '2020-01', '7722.00', '0.00', '0.00', 4, '2020-03-03 13:25:56'),
(6, 2, '2020-01', '6250.00', '0.00', '0.00', 4, '2020-03-03 13:26:30'),
(7, 1, '2020-01', '7722.00', '0.00', '0.00', 4, '2020-03-04 16:11:21'),
(8, 2, '2020-01', '6250.00', '0.00', '0.00', 4, '2020-03-05 07:10:00'),
(9, 1, '2020-01', '6735.91', '0.00', '8.21', 4, '2020-03-07 14:39:04'),
(10, 2, '2020-01', '6008.52', '0.00', '9.23', 4, '2020-03-07 15:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `stok_bibit`
--

CREATE TABLE `stok_bibit` (
  `id_stok` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `periode` varchar(10) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_bibit`
--

INSERT INTO `stok_bibit` (`id_stok`, `id_jenis_pepaya`, `periode`, `stok`) VALUES
(4, 1, '2019-01', 0),
(5, 1, '2019-02', 0),
(6, 1, '2019-03', 0),
(7, 1, '2019-04', 0),
(8, 1, '2019-05', 0),
(9, 1, '2019-06', 0),
(10, 1, '2019-07', 0),
(11, 1, '2019-08', 0),
(12, 1, '2019-09', 0),
(13, 1, '2019-10', 0),
(14, 1, '2019-11', 0),
(15, 1, '2019-12', 0),
(16, 2, '2019-01', 0),
(17, 2, '2019-02', 0),
(18, 2, '2019-03', 0),
(19, 2, '2019-04', 0),
(20, 2, '2019-05', 0),
(21, 2, '2019-06', 0),
(22, 2, '2019-07', 0),
(23, 2, '2019-08', 0),
(24, 2, '2019-09', 0),
(25, 2, '2019-10', 0),
(26, 2, '2019-11', 0),
(27, 2, '2019-12', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `stok_biji`
--

CREATE TABLE `stok_biji` (
  `id_stok` int(11) NOT NULL,
  `id_jenis_pepaya` int(11) NOT NULL,
  `periode` varchar(10) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_biji`
--

INSERT INTO `stok_biji` (`id_stok`, `id_jenis_pepaya`, `periode`, `stok`) VALUES
(1, 1, '2019-12', 100),
(2, 2, '2019-11', 130),
(3, 1, '2020-01', 150);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `role` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `no_telp` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `role`, `nama`, `no_telp`) VALUES
(1, 'pemilik', 'pemilik', 1, 'Satuyah', '0888888888'),
(3, 'penjualan', 'penjualan', 2, 'Admin Penjualan', ''),
(4, 'stok', 'stok', 3, 'Admin Stok', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history_stok_bibit`
--
ALTER TABLE `history_stok_bibit`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `history_stok_biji`
--
ALTER TABLE `history_stok_biji`
  ADD PRIMARY KEY (`id_history`);

--
-- Indexes for table `jenis_pepaya`
--
ALTER TABLE `jenis_pepaya`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`id_penjualan`);

--
-- Indexes for table `peramalan`
--
ALTER TABLE `peramalan`
  ADD PRIMARY KEY (`id_peramalan`);

--
-- Indexes for table `stok_bibit`
--
ALTER TABLE `stok_bibit`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `stok_biji`
--
ALTER TABLE `stok_biji`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history_stok_bibit`
--
ALTER TABLE `history_stok_bibit`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `history_stok_biji`
--
ALTER TABLE `history_stok_biji`
  MODIFY `id_history` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jenis_pepaya`
--
ALTER TABLE `jenis_pepaya`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `penjualan`
--
ALTER TABLE `penjualan`
  MODIFY `id_penjualan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `peramalan`
--
ALTER TABLE `peramalan`
  MODIFY `id_peramalan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `stok_bibit`
--
ALTER TABLE `stok_bibit`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `stok_biji`
--
ALTER TABLE `stok_biji`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
